<?php

class Animal {
    public $name;
    public $legs = 2;
    public $cold_blooded = false;

    public function __construct($_name)
    {
        $this->name = $_name;
    }
}

$sheep = new Animal("shaun");

echo $sheep->name; // "shaun"
echo "<br>";
echo $sheep->legs; // 2
echo "<br>";
echo $sheep->cold_blooded; // false
echo "<br>";

class Frog extends Animal {
    public function __construct()
    {
        $this->legs = 4;
    }
    public function jump(){
        echo "hop hop <br>";
    }
}

class Ape extends Animal {
    public function yell(){
        echo "Auooo <br>";
    }
}

$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"

$kodok = new Frog("buduk");
$kodok->jump() ; // "hop hop"

?>